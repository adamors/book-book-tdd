class CartController < ApplicationController
  def show
    @cart = CartPresenter.new(cart: cart_service.cart)
  end

  def update_quantity
    UpdateCartQuantity.call(cart: cart_service, params: update_cart_params)
    redirect_to view_cart_path, notice: 'Quantities updated'
  end

  def add_book
    AddToCart.call(cart: cart_service, params: params)
    redirect_to view_cart_path, notice: 'Added book to cart'
  end

  private

  def update_cart_params
    params.require(:line_item).permit(:id, :quantity)
  end
end
