class OrdersController < ApplicationController
  def address
    order
    @countries = CountryRepository.all
  end

  def save_address
    result = SaveAddress.call(session: session_service,
                              cart: cart_service,
                              params: save_address_params)
    if result.success?
      redirect_to orders_checkout_path
    else
      redirect_to add_order_address_path, alert: "Error saving order. #{result.errors}"
    end
  end

  def checkout
    @order = CalculateOrderCosts.call(order: order)
  end

  def finish_checkout
    @order.confirmed!
    @order.update_attribute(:user_id, @current_user.id)
    @order.update_book_sold_count
    OrderMailer.new_order(@order).deliver_now
    OrderMailer.new_customer_order(@order).deliver_now
    session.delete :order_id
    session.delete :cart_id
    redirect_to root_path, notice: 'Order sent successfully'
  end

  private

  def order
    @order ||= find_existing_order || Order.new
    @order
  end

  def find_existing_order
    OrderRepository.find(session[:order_id])
  end

  def save_address_params
    params.require(:order).permit(:name, :address, :country_id, :payment_type)
  end
end
