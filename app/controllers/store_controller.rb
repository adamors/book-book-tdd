class StoreController < ApplicationController
  def index
    @new_books = book_service.newest(limit: 10)
    @best_books = book_service.bestselling(limit: 10)
  end

  private

  def book_service
    @book_service ||= BookService.new
  end
end
