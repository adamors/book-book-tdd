class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  helper_method :user_presenter
  helper_method :categories

  def user_presenter
    @user_presenter ||= UserPresenter.new(session_service: session_service)
  end

  def session_service
    @session_service ||= SessionService.new(session: session)
  end

  def cart_service
    @cart_service ||= CartService.new(session: session)
  end

  def categories
    @categories ||= CategoryRepository.all
  end
end
