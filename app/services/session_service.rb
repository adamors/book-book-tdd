class SessionService
  attr_reader :session

  def initialize(session:)
    @session = session
  end

  def log_in(user)
    session[:user_id] = user.id
  end

  def current_user
    @current_user ||= UserRepository.find(session[:user_id])
  end

  def logged_in?
    !current_user.nil?
  end

  def log_in_admin(user)
    session[:admin_id] = user.id
  end

  def current_admin
    @current_admin ||= AdminRepository.find(session[:admin_id])
  end

  def admin_logged_in?
    !current_admin.nil?
  end

  def user_logged_in?
    logged_in? && !admin_logged_in?
  end
end
