class BookService
  def newest(limit:)
    books = BookRepository.newest(limit: limit)
    decorate_books(books)
  end

  def bestselling(limit:)
    books = BookRepository.bestselling(limit: limit)
    decorate_books(books)
  end

  private

  def decorate_books(books)
    books.map do |book|
      author = AuthorRepository.find(book.author_id)
      BookPresenter.new(book: book, author: author)
    end
  end
end
