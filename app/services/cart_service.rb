class CartService
  def initialize(session:)
    @session = session
  end

  def cart
    @cart ||= find_in_session
    @cart ||= create_new
  end

  def add(book:)
    line_item = LineItemRepository.find_by(book_id: book.id, cart_id: cart.id)
    unless line_item
      return LineItemRepository.new_from(cart_id: cart.id, book: book)
    end
    line_item.quantity += 1
    LineItemRepository.save(line_item)
  end

  def update_quantity(params:)
    line_item = LineItemRepository.find(params[:id])
    line_item.quantity = params[:quantity]
    if line_item.quantity <= 0
      LineItemRepository.delete line_item
    else
      LineItemRepository.save(line_item)
    end
  end

  private

  attr_reader :session

  def find_in_session
    CartRepository.find(session[:cart_id])
  end

  def create_new
    cart_entity = Cart.new
    CartRepository.save(cart_entity)
    session[:cart_id] = cart_entity.id
    cart_entity
  end
end
