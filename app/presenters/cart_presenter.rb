class CartPresenter
  delegate :empty?, :count, to: :line_items

  attr_reader :cart, :line_items

  def initialize(cart:)
    @cart = cart
    @line_items ||= LineItemRepository.for_cart(cart_id: cart.id)
  end

  def book_name(item)
    BookRepository.find(item.book_id).name
  end

  def total
    line_items.sum { |item| item.price * item.quantity }
  end
end
