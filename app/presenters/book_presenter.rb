class BookPresenter
  include ActiveModel::Model
  extend Forwardable

  delegate [:id, :name, :cover, :price] => :book

  attr_accessor :book, :author
end
