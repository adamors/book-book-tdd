class UserPresenter
  def initialize(session_service:)
    @session_service = session_service
  end

  def my_account(view_context:)
    if session_service.user_logged_in?
      view_context.render 'header_user_account'
    elsif session_service.admin_logged_in?
      view_context.render 'header_admin_account'
    else
      view_context.render 'header_login'
    end
  end

  private

  attr_reader :session_service
end
