class BookRepository
  extend Db::Delegation

  def self.bestselling(limit:)
    all.sort_by(&:sold_count).reverse.take(limit)
  end

  def self.newest(limit:)
    all.sort_by(&:created_at).take(limit)
  end
end
