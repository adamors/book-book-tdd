class LineItemRepository
  extend Db::Delegation

  def self.find_by(book_id:, cart_id:)
    proc = proc do |line_item|
      line_item.book_id == book_id && line_item.cart_id == cart_id
    end
    filter(proc).first
  end

  def self.for_cart(cart_id:)
    filter(->(line_item) { line_item.cart_id == cart_id })
  end

  def self.new_from(cart_id:, book:)
    book = LineItem.new(cart_id: cart_id, book_id: book.id, price: book.price)
    save(book)
    book
  end

  private_class_method

  def self.filter(condition)
    all.select(&condition)
  end
end
