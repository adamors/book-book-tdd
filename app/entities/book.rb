class Book
  include ActiveModel::Model

  attr_accessor :id, :name, :isbn, :cover, :price, :category_id,
                :author_id, :created_at, :sold_count
end
