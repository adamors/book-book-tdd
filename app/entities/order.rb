class Order
  include ActiveModel::Model

  attr_accessor :id, :user_id, :name, :country_id, :payment_type, :total, :tax, :shipping_cost

  AMOUNT_FOR_FREE_SHIPPING = 100

  def total_with_tax
    total + tax
  end

  def tax
    @tax || 0
  end

  def total
    @total || 0
  end

  def shipping_cost
    @shipping_cost || 0
  end
end
