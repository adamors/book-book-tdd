class Result
  attr_reader :errors, :record

  def initialize(errors: nil, record: nil)
    @errors = errors
    @record = record
  end

  def success?
    errors.nil? || errors.empty?
  end
end
