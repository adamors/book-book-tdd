class Country
  include ActiveModel::Model

  attr_accessor :id, :name, :tax

  def tax
    @tax || 0
  end
end

