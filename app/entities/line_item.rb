class LineItem
  include ActiveModel::Model

  attr_accessor :id, :quantity, :price, :book_id, :cart_id, :order_id

  def initialize(attributes = {})
    super
    @quantity = 1
  end

  def quantity
    @quantity.to_i
  end
end
