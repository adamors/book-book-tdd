class CalculateOrderCosts
  def call
    order.tax = order.total * country.tax / 100.0
    order.shipping_cost = shipping_cost
    OrderRepository.save(order)
  end

  def self.call(order:)
    new(order).call
  end

  private

  attr_reader :order

  def initialize(order)
    @order = order
  end

  def country
    @country ||= CountryRepository.find(order.country_id)
  end

  def total
    line_items.to_a.sum { |item| item.price * item.quantity }
  end

  def shipping_cost
    order.total_with_tax > Order::AMOUNT_FOR_FREE_SHIPPING ? 0 : 10
  end
end
