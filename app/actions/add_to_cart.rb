# Action for adding a book to the cart
class AddToCart
  def call
    cart.add(book: book)
  end

  def self.call(cart:, params:)
    new(cart, params).call
  end

  private

  attr_reader :cart, :params

  def initialize(cart, params)
    @params = params
    @cart = cart
  end

  def book
    BookRepository.find(params[:book_id])
  end
end
