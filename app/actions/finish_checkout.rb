class Something
  def deliver
    puts 'delivering'
  end
end

class OrderMailer
  def self.new_order(order)
  end
end

class FinishCheckout
  def call
    OrderMailer.new_order(order)
    update_order
    session_service.empty_current_order
  end

  def self.call(session_service:)
    new(session_service).call
  end

  private

  attr_reader :session_service

  def initialize(session_service)
    @session_service = session_service
  end

  def order
    @order ||= session_service.current_order
  end

  def update_order
    order.user_id = session_service.current_order.id
    OrderRepository.save(order)
  end
end
