class UpdateCartQuantity
  def call
    cart.update_quantity(params: params)
  end

  def self.call(cart:, params:)
    new(cart, params).call
  end

  private

  attr_reader :cart, :params

  def initialize(cart, params)
    @params = params
    @cart = cart
  end
end
