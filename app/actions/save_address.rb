# Action for adding a book to the cart
class SaveAddress
  def call
    save_address
    update_cart_contents
    store_in_session
    Result.new
  end

  def self.call(session:, params:, cart:)
    new(session, cart, params).call
  end

  private

  attr_reader :session, :params, :cart

  def initialize(session, cart, params)
    @params = params
    @session = session
    @cart = cart
  end

  def save_address
    order.assign_attributes(params)
    return Result.new(errors: order.errors.full_messages.join(', ')) unless OrderRepository.save(order) || errors.empty?
  end

  def update_cart_contents
    line_items = LineItemRepository.for_cart(cart_id: cart.cart.id)
    line_items.each do |item|
      item.order_id = order.id
      LineItemRepository.save(item)
    end
  end

  def store_in_session
    session.session[:order_id] = order.id
  end

  def order
    order_id = session.session[:order_id]
    @order ||= OrderRepository.find(order_id) || Order.new
  end
end
