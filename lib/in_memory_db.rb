class InMemoryDb
  @storage = {}

  def self.create(record)
    record.id ||= counter_for(record)
    storage_for(record)[record.id] = record
  end

  def self.count(record)

  end

  def self.update(record)
    storage_for(record)[record.id] = record
  end

  def self.delete(record)
    storage_for(record).delete record.id
  end

  def self.find(klass, id)
    storage_for_class(klass).fetch(id.to_i, nil)
  end

  def self.all(klass)
    storage_for_class(klass).values
  end

  def self.query(klass, selector)
    send "query_#{selector.class.name.underscore}", selector
  end

  def self.delete_all(klass)
    @storage[klass.to_s.to_sym] = {}
  end

  private_class_method

  def self.storage_for_class(klass)
    @storage[klass.to_s.to_sym] ||= {}
  end

  def self.storage_for(record)
    storage_for_class(record.class)
  end

  def self.counter_for(record)
    storage_for_class(record.class).count + 1
  end
end
