module Db
  class Repository
    def self.adapter
      @adapter
    end

    def self.adapter=(adapter)
      @adapter = adapter
    end

    def self.find(klass, id)
      adapter.find klass, id
    end

    def self.all(klass)
      adapter.all klass
    end

    def self.count(klass)
      adapter.all(klass).count
    end

    def self.create(model)
      adapter.create(model)
    end

    def self.update(model)
      adapter.update(model)
    end

    def self.save(model)
      if model.id
        update(model)
      else
        create(model)
      end
    end

    def self.delete(model)
      adapter.delete model
    end

    def self.delete_all(klass)
      adapter.delete_all klass
    end
  end
end
