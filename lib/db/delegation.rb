module Db
  module Delegation
    def save(record)
      Repository.save(record)
    end

    def all
      Repository.all object_class
    end

    def count
      Repository.count object_class
    end

    def find(id)
      Repository.find object_class, id
    end

    def delete(record)
      Repository.delete record
    end

    def delete_all
      Repository.delete_all object_class
    end

    private

    def object_class
      @object_class ||= self.to_s.match(/^(.+)Repository/)[1].constantize
    end
  end
end

