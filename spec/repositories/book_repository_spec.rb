require 'rails_helper'

RSpec.describe BookRepository do
  describe '#create' do
    it 'creates a record' do
      book = Book.new(name: 'How to')
      result = BookRepository.save(book)
      found_book = BookRepository.find(1)
      result.should == found_book
    end

    it 'add a new id for record' do
      author = Author.new(name: 'How to')
      result = AuthorRepository.save(author)
      found_author = AuthorRepository.find(1)
      result.should == found_author
    end
  end
end
