require 'rails_helper'

RSpec.describe UserPresenter do
  describe '#my_account' do
    let(:session_service) { double('session') }
    let(:view_context) { double('view_context') }

    subject { UserPresenter.new(session_service: session_service) }

    context 'when user is logged in' do
      before do
        allow(session_service).to receive(:user_logged_in?).and_return(true)
      end

      it 'renders user account template' do
        view_context.should receive(:render).with('header_user_account')
        subject.my_account(view_context: view_context)
      end
    end
  end
end
