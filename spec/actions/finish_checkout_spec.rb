require 'rails_helper'

RSpec.describe FinishCheckout do
  describe '#call' do
    let(:mailer) { class_double('OrderMailer').as_stubbed_const }
    let(:session) { double('session') }
    let(:order) { Order.new(id: 1) }
    let(:user) { User.new(id: 1) }

    subject { FinishCheckout.call(session_service: session) }

    before do
      allow(session).to receive(:current_user).and_return(user)
      allow(session).to receive(:current_order).and_return(order)
      allow(session).to receive(:empty_current_order)
      allow(mailer).to receive(:new_order).with(order)
    end

    it 'sends an email' do
      mailer.should receive(:new_order).with(order)
      subject
    end

    it 'deletes the current order from the session' do
      session.should receive(:empty_current_order)
      subject
    end

    it 'assigns the order to the current user' do
      subject
      order.user_id.should == user.id
    end
  end
end
