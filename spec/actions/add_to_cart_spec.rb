require 'rails_helper'

RSpec.describe AddToCart do
  describe '#call' do
    let(:cart) do
      cart = instance_double('CartService')
      allow(cart).to receive(:add)
      cart
    end
    let(:params) { { book_id: 1 } }

    subject { AddToCart.call(cart: cart, params: params) }

    it 'adds a book to the cart' do
      cart.should receive(:add)
      subject
    end
  end
end
