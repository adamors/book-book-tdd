require 'rails_helper'

RSpec.describe CartController, type: :controller do
  describe '#add_book' do
    let(:book) { Book.new(id: 1) }

    before { allow(BookRepository).to receive(:find).and_return(book) }

    subject { get :add_book, params: { book_id: book.id } }

    it 'redirects to cart path' do
      subject.should redirect_to(view_cart_path)
    end
  end

  describe '#update_quantity' do
    let(:line_item) { LineItem.new(id: 1) }

    subject { get :update_quantity, params: { line_item: { id: line_item.id, quantity: 2 } } }

    it 'redirects to cart path' do
      subject.should redirect_to(view_cart_path)
    end
  end
end
