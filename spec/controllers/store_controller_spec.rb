require 'rails_helper'

RSpec.describe StoreController, type: :controller do
  describe '#index' do
    it 'renders a list of books' do
      get :index
      response.should have_http_status(200)
    end
  end
end
