require 'rails_helper'

RSpec.describe OrdersController, type: :controller do
  describe '#address' do
    it 'returns 200 status' do
      get :address
      response.should have_http_status(200)
    end
  end

  describe '#save_address' do
    context 'when successful result' do
      it 'redirects to checkout' do
        post :save_address, params: { order: { name: 'Foo' } }
        response.should redirect_to orders_checkout_path
      end
    end

    context 'when unsuccessful result' do
      before { allow(SaveAddress).to receive(:call).and_return(Result.new(errors: 'foo')) }

      it 'redirects to checkout' do
        post :save_address, params: { order: { name: 'Foo' } }
        response.should redirect_to add_order_address_path
      end
    end
  end

  describe '#checkout' do
    let(:country) { Country.new }
    let(:order) { Order.new }

    before do
      CountryRepository.save(country)
      order.country_id = country.id
      OrderRepository.save(order)
    end

    it 'returns 200 status' do
      get :checkout, session: { order_id: order.id }
      response.should have_http_status(200)
    end
  end
end
