require 'rails_helper'

RSpec.describe AddToCart do
  let(:session) { {} }

  before(:all) { LineItemRepository.delete_all }

  subject { CartService.new(session: session) }

  describe '#add' do
    context "when book isn't in the cart yet" do
      let(:book) { Book.new(id: 1) }
      let(:cart) { Cart.new(id: 1) }

      before do
        allow(CartRepository).to receive(:find).and_return(cart)
      end

      it 'creates a new line item' do
        subject.add(book: book)
        LineItemRepository.count.should == 1
      end
    end

    context 'when book is in the cart already' do
      let(:book) { Book.new(id: 1) }
      let(:cart) { Cart.new(id: 1) }

      before do
        allow(CartRepository).to receive(:find).and_return(cart)
      end

      it 'updates an existing line item' do
        subject.add(book: book)
        line_item = LineItemRepository.find_by(cart_id: cart.id, book_id: book.id)
        line_item.quantity.should == 2
      end
    end
  end

  describe 'update_quantity' do
    let(:line_item) { LineItem.new(id: 1) }

    context 'when quantity is 0' do
      it 'deletes line item from repository' do
        LineItemRepository.should receive(:delete)
        subject.update_quantity(params: { id: line_item.id, quantity: 0 })
      end
    end

    context 'when quantity is not 0' do
      it "updates line item's quantity" do
        subject.update_quantity(params: { id: line_item.id, quantity: 2 })
        LineItemRepository.find(line_item.id).quantity.should == 2
      end
    end
  end

  describe '#cart' do
    context "when cart doesn't exist in session" do
      it 'creates a new cart object' do
        CartRepository.should receive(:save)
        cart = subject.cart
        cart.should be_a(Cart)
      end

      it 'saves it in the session' do
        cart = subject.cart
        session[:cart_id].should == cart.id
      end
    end

    context 'when cart exists in the session' do
      let(:cart) { Cart.new }

      before do
        CartRepository.save(cart)
        session[:cart_id] = cart.id
      end

      it 'finds it in the session' do
        returned_cart = subject.cart
        returned_cart.should == cart
      end
    end
  end
end
