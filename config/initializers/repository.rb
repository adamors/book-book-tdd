Db::Repository.adapter = InMemoryDb

unless Rails.env.test?
  categories = [
    'Computing', 'Entertainment', 'Fiction', 'Technology & Engineering'
  ]

  unless CategoryRepository.count > 0
    categories.each do |category_name|
      CategoryRepository.save(Category.new(name: category_name))
    end
  end

  unless AuthorRepository.count > 0
    1.upto(3) do |_|
      author = Author.new(name: Faker::Book.author)
      AuthorRepository.save(author)
    end
  end

  unless BookRepository.count > 0
    category_count = CategoryRepository.count
    author_count = AuthorRepository.count
    1.upto(10) do |_|
      category_id = rand(1..category_count)
      author_id = rand(1..author_count)
      isbn = Faker::Code.isbn
      image = RubyIdenticon.create_base64(isbn)
      book =  Book.new(
        name: Faker::Book.title,
        isbn: isbn,
        category_id: category_id,
        author_id: author_id,
        cover: image,
        price: Faker::Commerce.price,
        created_at: DateTime.now
      )
      BookRepository.save(book)
    end
  end
end
