Rails.application.routes.draw do
  root to: 'store#index'
  resources :books
  resources :authors
  resources :categories
  resources :users
  resources :sessions, only: [:new, :create]

  get 'cart/add/:book_id', to: 'cart#add_book', as: 'add_to_cart'
  patch 'cart/update-quantity', to: 'cart#update_quantity', as: 'update_cart_quantity'
  get 'cart/show', to: 'cart#show', as: 'view_cart'

  delete 'logout', to: 'sessions#destroy'
  get 'store/index', as: 'store_index'

  get 'settings', to: 'settings#index'
  get 'orders/address', to: 'orders#address', as: 'add_order_address'
  get 'orders/index', to: 'orders#index', as: 'orders'
  post 'orders/address', to: 'orders#save_address', as: 'save_order_address'
  get 'orders/checkout', to: 'orders#checkout', as: 'orders_checkout'
  get 'orders/:id', to: 'orders#show', as: 'order'
  post 'orders/finish', to: 'orders#finish_checkout', as: 'finish_checkout'
  get '/admin', to: 'admin_session#new'
  post '/admin', to: 'admin_session#create', as: 'admin_session'
  get '/admin/orders', to: 'admin#orders', as: 'admin_orders'
  get '/admin/users', to: 'admin#users', as: 'admin_users'
end
