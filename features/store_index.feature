Feature: Store index
  In order to shop
  As a visitor
  I want to see new releases and bestsellers

Scenario: New releases list
    Given the store has new releases
    When a visitor visits the index
    Then the new releases should be displayed

Scenario: Bestsellers list
    Given the store has bestsellers
    When a visitor visits the index
    Then the bestsellers should be displayed

Scenario: Adding books to cart
    Given the book I look for exists
    When I click on the "Add to cart" button
    Then the book gets added to the cart
    And I am taken to the cart page

Scenario: Login
    Given I am a visitor
    When I visit the index page
    Then I should see the login link

Scenario: My Account
    Given I am logged in
    When I visit the index page
    Then I should have access to the "Account" menu
