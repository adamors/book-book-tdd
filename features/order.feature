Feature: Order
  In order to buy books
  As a logged in user
  I want to create orders

Scenario: Finish Order
    Given I've completed the Checkout page
    When I click the "Finish Order" button
    Then I should receive a confirmation email
    And my cart should be emptied
    And I should be redirected to the index page
