Given(/^the store has new releases$/) do
  @author = Author.new(name: 'Author')
  AuthorRepository.save(@author)
  @book = Book.new(name: 'Book', sold_count: 0, author_id: @author.id, created_at: DateTime.now)
  BookRepository.save(@book)
end

When(/^a visitor visits the index$/) do
  visit root_path
end

Then(/^the new releases should be displayed$/) do
  page.should have_text('New releases')

  new_releases_title = all(:css, '.new-releases .book-title').map(&:text)
  new_releases_title.should =~ [@book.name]
end

Given(/^the store has bestsellers$/) do
  @author = Author.new(name: 'Author')
  AuthorRepository.save(@author)
  @bestsellers = []
  1.upto(10) do |n|
    book = Book.new(name: "Sold Book #{n}", sold_count: n, author_id: @author.id, created_at: DateTime.now)
    BookRepository.save(book)
    @bestsellers << book
  end
end

Then(/^the bestsellers should be displayed$/) do
  best_sellers_title = all(:css, '.best-sellers .book-title').map(&:text)
  best_sellers_title.should =~ @bestsellers.map(&:name)
end
